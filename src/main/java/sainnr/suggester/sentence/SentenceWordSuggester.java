package sainnr.suggester.sentence;

import com.sun.org.apache.xpath.internal.operations.String;

import java.util.List;

/**
 * @author vsalin
 * <p/>25.05.2018
 */
public interface SentenceWordSuggester {

    /**
     * Accounts provided sentence for the statistics of further suggestions. Can use a weighted graph
     * for it to potentially support fuzzy suggestions.
     */
    void addSentence(String text);

    /**
     * Suggests the next word based on a sequence of previous words used and previously accounted
     * sentences. Does a lookup in the weighted graph for a path with all words and returns all next
     * nodes if exact match is found. If no exact match, then looks for alt routes from 1st word
     * in a provided sequence to the last one with max occurence of words between and gives next nodes
     * as suggestions.
     */
    List<String> suggestWord(List<String> previousWords);
}

package sainnr.suggester.prefix;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.PriorityQueue;

/**
 * <p>A prefix tree based implementation (<a href="https://en.wikipedia.org/wiki/Trie">Trie</a>).</p>
 *
 * <p>Performs insertions, counting and removal with a single loop.
 * The underlying {@link TrieNode} uses a <code>HashMap</code> for tracking its children with O(1)
 * insertion and lookup (except rare cases of map resize with O(n), where n is map size).</p>
 *
 * <p>Words suggestion algorithm traverses the trie in breadth and prioritises high-weighted nodes
 * with max-heap. It uses a wrapper {@link RankedSuggestion} to track the char path to the current
 * node and build the word on the fly. Approx time complexity is O(P + K*Ri*log(|C|)) where
 * P is length of the prefix, K is a limit of suggestions, Ri is a remainder length of i-th word
 * and |C| is a total size of remaining prefix tree.
 * With 1-char prefix and all different words of length N it roughly comes to O(K*N*log(|C|)).</p>
 *
 * <p>Size complexity should be of O(|C|) where |C| is a total size of prefix tree.</p>
 *
 * <p>Alternative data structure could be a graph with nodes representing characters and weighted
 * edges for counting usage. In such a case, it'd be possible to apply a modified Dijkstra algorithm
 * on it (with max-heap and path tracking) for the suggestions lookup.</p>
 *
 * <p>Code is covered with unit tests to ensure general & some corner cases.</p>
 *
 * @author vsalin
 * <p/>25.05.2018
 */
public class TrieSuggester implements AutoSuggester {

    private final TrieNode head;

    public TrieSuggester() {
        this.head = new TrieNode('*'); // dummy root
    }

    @Override
    public void addWord(String newWord) {
        if (newWord == null) {
            return;
        }
        newWord = newWord.trim(); // might be avoided if we're sure that input is free of " a "

        TrieNode cur = head;
        for (int i = 0; i < newWord.length(); i++) {
            char curCh = newWord.charAt(i);
            cur = (i == newWord.length() - 1)
                    ? cur.addNext(curCh, true)
                    : cur.addNext(curCh);
        }
    }

    @Override
    public int countWord(String word) {
        if (word == null || word.isEmpty()) {
            return 0;
        }
        word = word.trim();

        int cnt = 0;
        TrieNode cur = head;
        for (int i = 0; i < word.length(); i++) {
            char curCh = word.charAt(i);
            if (cur.hasNext(curCh)) {
                cur = cur.next(curCh);
                if (i == word.length() - 1) {
                    cnt = cur.count;
                }
            } else {
                // there's no such word in a trie
                return 0;
            }
        }
        return cnt;
    }

    // Also supports removing any word that in turn would decrease the ranking of
    // corresponding chars left in the container.
    @Override
    public void removeWord(String obsoleteWord) {
        if (obsoleteWord == null) {
            return;
        }
        obsoleteWord = obsoleteWord.trim();

        TrieNode cur = head;
        for (char c : obsoleteWord.toCharArray()) {
            if (cur.hasNext(c)) {
                TrieNode next = cur.next(c);
                if (next.count == 1) {
                    cur.removeNext(c);
                } else {
                    next.count--;
                    cur = next;
                }
            } // else there's no such word in a trie
        }
    }

    @Override
    public List<String> suggestWords(String prefix, int maxSuggestions) {
        if (prefix == null) {
            return new LinkedList<>();
        }
        prefix = prefix.trim();
        if (prefix.isEmpty()) {
            return new LinkedList<>();
        }
        // prefix.toLowerCase() can be used if there's a need for case-insensitive support

        TrieNode cur = head;
        for (char c : prefix.toCharArray()) {
            if (cur.hasNext(c)) {
                cur = cur.next(c);
            } else {
                // no words for such prefix
                return new LinkedList<>();
            }
        }
        if (cur.allNext().isEmpty()) {
            return Collections.singletonList(prefix);
        }
        return findWordsWithPrefix(prefix, cur.allNext(), maxSuggestions);
    }

    private List<String> findWordsWithPrefix(String prefix, Collection<TrieNode> allNext, int limit) {
        List<String> suggestions = new LinkedList<>();

        // initialise a max heap with starter set of nodes
        PriorityQueue<RankedSuggestion> maxHeap = new PriorityQueue<>(
                allNext.size(),
                (n1, n2) -> n2.weight - n1.weight
        );
        addToHeap(allNext, maxHeap, new StringBuilder(prefix));

        // get max weight suggestion and update its sb that track a "path" (word)
        while (!maxHeap.isEmpty() && suggestions.size() < limit) {
            RankedSuggestion rs = maxHeap.poll();
            TrieNode node = rs.node;
            rs.sb.append(node.value);

            if (node.endOfWord) {
                suggestions.add(rs.sb.toString());
            }
            addToHeap(node.allNext(), maxHeap, rs.sb);
        }
        return suggestions;
    }

    private void addToHeap(Collection<TrieNode> allNext, PriorityQueue<RankedSuggestion> heap, StringBuilder sb) {
        for (TrieNode node : allNext) {
            RankedSuggestion rs = new RankedSuggestion(new StringBuilder(sb), node);
            heap.add(rs);
        }
    }

    private class RankedSuggestion {

        private StringBuilder sb;
        private TrieNode node;
        private int weight;

        private RankedSuggestion(StringBuilder sb, TrieNode node) {
            this.sb = sb;
            this.node = node;
            this.weight = node.count;
        }
    }

    private class TrieNode {

        private char value;
        private int count;
        private Map<Character, TrieNode> nextNodes;
        private boolean endOfWord;

        private TrieNode(char value) {
            this.value = value;
            this.count = 0;
            this.nextNodes = new HashMap<>();
            this.endOfWord = false;
        }

        private TrieNode next(char val) {
            return nextNodes.get(val);
        }

        private Collection<TrieNode> allNext() {
            return nextNodes.values();
        }

        private boolean hasNext(char val) {
            return nextNodes.containsKey(val);
        }

        private TrieNode addNext(char val) {
            return addNext(val, false);
        }

        private TrieNode addNext(char val, boolean isEnd) {
            TrieNode next = hasNext(val) ? next(val) : new TrieNode(val);
            if (isEnd) {
                next.endOfWord = true;
            }
            next.count++;
            nextNodes.put(val, next);
            return next;
        }

        private void removeNext(char val) {
            nextNodes.remove(val);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof TrieNode)) return false;
            TrieNode trieNode = (TrieNode) o;
            return value == trieNode.value;
        }

        @Override
        public int hashCode() {
            return Objects.hash(value);
        }
    }
}

package sainnr.suggester.prefix;

import java.util.List;

/**
 * <p>Enhanced version of {@link TrieSuggester} with a slightly different words suggestion impl.</p>
 *
 * <p>Instead of default ranked wrapper, it can use either a {@link ClassNameSuggestion} wrapper
 * for class names or {@link VariableNameSuggestion} for var names. Such wrappers would hold
 * a skip rule that could be used in main trie traversal loop to find all suitable options in it.
 * They can be chosen within the constructor of IdeSuggester (e.g. with Enum).</p>
 *
 * @author vsalin
 * <p/>25.05.2018
 */
public class IdeSuggester extends TrieSuggester {

    @Override
    public List<String> suggestWords(String prefix, int maxSuggestions) {
        /*
            Similar to the default trie suggester, use weighted traversal but also consider
            a skip rule embedded in a proper name suggester (of two below).
         */

        // todo
        return null;
    }

    /**
     * A skip rule would be to the next capital letter in a prefix
     */
    private class ClassNameSuggestion {}

    /**
     * Might skip from current char in prefix to the "_"
     */
    private class VariableNameSuggestion {}

}

package sainnr.suggester.prefix;

import java.util.List;

/**
 * @author vsalin
 * <p/>25.05.2018
 */
public interface AutoSuggester {
    /**
        Add a single word to an internal dictionary as a potential suggestion.
        This is called when parsing the document prior to modification by user,
        and each time a new word is typed (as indicated by a space).
     */
    void addWord(String newWord);

    /**
        Return the number of times a given word has been typed within the
        document.
     */
    int countWord(String word);

    /**
        Remove a word from the internal dictionary. This is called when a user
        has deleted or modified a word in the document, whose CountWord() output
        was previously 1.
     */
    void removeWord(String obsoleteWord);

    /**
        Given a prefix return a list of at most max_suggestions suggested words in
        descending order of frequency within the document
     */
    List<String> suggestWords(String prefix, int maxSuggestions);
}

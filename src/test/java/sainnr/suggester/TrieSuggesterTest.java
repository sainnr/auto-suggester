package sainnr.suggester;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;

import sainnr.suggester.prefix.AutoSuggester;
import sainnr.suggester.prefix.TrieSuggester;

/**
 * @author vsalin
 * <p/>25.05.2018
 */
public class TrieSuggesterTest {

    private AutoSuggester suggester;

    @Before
    public void setUp() throws Exception {
        suggester = new TrieSuggester();

        suggester.addWord("Timmy");
        suggester.addWord("Timmie");
        suggester.addWord("Tim");
        suggester.addWord("Times");
        suggester.addWord("TIEFighter");
        suggester.addWord("time");
        suggester.addWord("Tom");
        suggester.addWord("TomTom");
        suggester.addWord("toe");
    }

    @Test
    public void shouldAddWord() {
        suggester.addWord("Hello");
        suggester.addWord("darkness");
        suggester.addWord(" my");
        suggester.addWord("old ");
        suggester.addWord("  friend ");
    }

    @Test
    public void shouldCountWord() {
        int count = suggester.countWord("Tim");
        Assert.assertEquals(4, count);

        count = suggester.countWord("times");
        Assert.assertEquals(0, count);
    }

    @Test
    public void shouldRemoveWord() {
        Assert.assertEquals(4, suggester.countWord("Tim"));

        suggester.removeWord("Times");
        Assert.assertEquals(3, suggester.countWord("Tim"));

        suggester.removeWord("time");
        Assert.assertEquals(3, suggester.countWord("Tim"));
    }

    @Test
    public void shouldSuggestWords() {
        Collection<String> res = suggester.suggestWords("Ti", 10);
        System.out.println("Ti:");
        System.out.println(String.join("\n", res));
        System.out.println();

        Assert.assertEquals(4, res.size());
        Assert.assertTrue(res.contains("Tim"));
        Assert.assertTrue(res.contains("Times"));
        Assert.assertTrue(res.contains("Timmy"));
        Assert.assertTrue(res.contains("Timmie"));


        res = suggester.suggestWords("T", 3);
        System.out.println("T:");
        System.out.println(String.join("\n", res));
        System.out.println();

        Assert.assertEquals(3, res.size());


        res = suggester.suggestWords("tic", 3);
        System.out.println("tic:");
        System.out.println(String.join("\n", res));

        Assert.assertEquals(0, res.size());
    }
}

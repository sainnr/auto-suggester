package sainnr.suggester;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import sainnr.suggester.prefix.AutoSuggester;
import sainnr.suggester.prefix.TrieSuggester;

/**
 * @author vsalin
 * <p/>25.05.2018
 */
public class SampleTextTest {

    private static final String TEXT =
            "Dogmatic use of catapults by catatonic " +
            "operators results in dire consequences";

    private static final String[] PREFIXES = new String[] {"do","cat","catat","shoe"};

    private AutoSuggester suggester;

    @Before
    public void setUp() throws Exception {
        suggester = new TrieSuggester();
        for (String s : TEXT.toLowerCase().split(" ")) {
            suggester.addWord(s);
        }
    }

    @Test
    public void shouldSupportPrefixes() {
        List<String> res = suggester.suggestWords(PREFIXES[0], 5);
        Assert.assertEquals(1, res.size());
        Assert.assertTrue(res.contains("dogmatic"));

        res = suggester.suggestWords(PREFIXES[1], 5);
        Assert.assertEquals(2, res.size());
        Assert.assertTrue(res.contains("catatonic"));
        Assert.assertTrue(res.contains("catapults"));

        res = suggester.suggestWords(PREFIXES[2], 5);
        Assert.assertEquals(1, res.size());
        Assert.assertTrue(res.contains("catatonic"));

        res = suggester.suggestWords(PREFIXES[3], 5);
        Assert.assertEquals(0, res.size());
    }
}
